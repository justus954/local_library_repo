var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AuthorSchema = new Schema(
  {
    first_name: {type: String, required: true, max: 100},
    family_name: {type: String, required: true, max: 100},
    date_of_birth: {type: Date},
    date_of_death: {type: Date},
  }
);

// Virtual for author's full name
AuthorSchema
.virtual('name')
.get(function () {
  return this.family_name + ', ' + this.first_name;
});

// Virtual for author's lifespan
AuthorSchema
.virtual('lifespan')
.get(function () {

  

  if(this.date_of_birth!=null && this.date_of_death!=null){
    return (this.date_of_death.getYear() - this.date_of_birth.getYear()).toString();
  }

  if(this.date_of_birth!=null){
    return ((new Date()).getYear() - this.date_of_birth.getYear()).toString();
  }
  
  if(this.date_of_death!=null){
    return "Date of death: "+this.date_of_death.getDate()+" "+this.date_of_death.getMonth()+" "+this.date_of_death.getFullYear();
  }

  return "No data";
});

// Virtual for author's URL
AuthorSchema
.virtual('deathdate_formformat')
.get(function () {
  if(this.date_of_death!=null){
    deathday = this.date_of_death.getDate();
    deathmonth = this.date_of_death.getMonth();

    if(deathday<10)
      deathday="0"+deathday;

    if(deathmonth<10)
      deathmonth="0"+deathmonth;

    return this.date_of_death.getFullYear()+"-"+deathmonth+"-"+deathday;
  }else{
    return null;
  }
});

// Virtual for author's URL
AuthorSchema
.virtual('birthdate_formformat')
.get(function () {
  if(this.date_of_birth!=null){
    birthday = this.date_of_birth.getDate();
    birthmonth = this.date_of_birth.getMonth();

    if(birthday<10)
      birthday="0"+birthday;

    if(birthmonth<10)
      birthmonth="0"+birthmonth;

    return this.date_of_birth.getFullYear()+"-"+birthmonth+"-"+birthday;
  }else{
    return null;
  }
});

// Virtual for author's URL
AuthorSchema
.virtual('url')
.get(function () {
  return '/catalog/author/' + this._id;
});

//Export model
module.exports = mongoose.model('Author', AuthorSchema);